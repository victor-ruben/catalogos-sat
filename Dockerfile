FROM ruby:2.5.1

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev

RUN mkdir /catalogos-sat

WORKDIR /catalogos-sat

COPY Gemfile /catalogos-sat/Gemfile

COPY Gemfile.lock /catalogos-sat/Gemfile.lock

RUN bundle install

COPY . /catalogos-sat

EXPOSE 3000

CMD ["bundle", "exec", "rails", "server", "-b", "0.0.0.0"]