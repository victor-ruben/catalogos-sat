# catalogos-sat
---

### Run app via docker-compose
Step 2 and 3 only needed the first time.

1. Initialize services
	```
	$ docker-compose up
	```



2. Create db with commands
	```
	$ docker-compose run api rake db:create
	```


3. Since psql is not installed on the ruby image, run directly on the db container

	- 3.1 Only db structure:

		```
		$ docker exec -i DB_CONTAINER_ID psql -U postgres -W catalogos-sat_development < db/structure.sql
		```

	- 3.2 db structure and catalog data (Use latest DATE-db-structure-with-data.sql file you find on the db directory)

		```
		$ docker exec -i DB_CONTAINER_ID psql -U postgres -W catalogos-sat_development < db/20180916-db-structure-with-data.sql
		```


4. The app should be accessible now at http://localhost:3000/ to check data is available go to http://localhost:3000/v1/categoria/division	


### Run rspec via docker-compose

1. If first time running specs, create and migrate the test database

	```
  $ docker-compose run --rm -e RAILS_ENV=test api rake db:create
  $ docker-compose run --rm -e RAILS_ENV=test api rake db:migrate
	```

2. Run rspec

  ```
  $ docker-compose run --rm api bundle exec rspec
  ```

### Run rubocop via docker-compose

  ```
  docker-compose run --rm api bundle exec rubocop
  ```