module Api
  module V1
    class ApiController < ApplicationController
      # Overrides method of PagerApi gem, so we can use it to set response haeaders
      # and to pass options to fast_jsonapi serializers
      # Usage example: Add to last page of a controller action
      #  paginate items, ::V1::CatalogoProdServSerializer, per_page: per_page
      def paginate(collection, serializer, options = {})
        paginated_collection = collection.page(params[:page]).per(options[:per_page])

        serializer_options = {}

        if PagerApi.include_pagination_on_meta?
          serializer_options[:meta] = meta(paginated_collection, options)
        end

        pagination_headers(paginated_collection) if PagerApi.include_pagination_headers?

        render json: serializer.new(paginated_collection, serializer_options)
      end
    end
  end
end
