module Api
  module V1
    class CatalogoClaveUnidadController < ApiController
      include Pagination

      FILTERS_WHITE_LIST = %w[c_clave_unidad
                              nombre
                              descripcion
                              nota
                              simbolo
                              fecha_inicio_vigencia
                              fecha_fin_vigencia].freeze

      # rubocop:disable Metrics/MethodLength
      def index
        items = CatalogoClaveUnidad
                .select(:id,
                        :c_clave_unidad,
                        :nombre,
                        :descripcion,
                        :nota,
                        :fecha_inicio_vigencia,
                        :fecha_fin_vigencia,
                        :simbolo)
                .add_order(params[:sorted])
                .filter(params[:filtered], FILTERS_WHITE_LIST)

        per_page = pagination_per_page(params[:pageSize])

        paginate items, ::V1::CatalogoClaveUnidadSerializer, per_page: per_page
      end
      # rubocop:enable Metrics/MethodLength
    end
  end
end
