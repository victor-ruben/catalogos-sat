module Api
  module V1
    class CatalogoProdServsController < ApiController
      include Pagination

      FILTERS_WHITE_LIST = %w[clave_prod_serv
                              descripcion
                              fecha_inicio_vigencia
                              fecha_fin_vigencia
                              incluir_iva_trasladado
                              incluir_ieps_trasladado
                              descripcion_clase
                              descripcion_grupo
                              descripcion_division
                              tipo_id
                              division_id
                              grupo_id
                              clase_id].freeze
      # rubocop:disable Metrics/MethodLength
      def index
        items = CatalogoProdServ
                .select(:id,
                        :clave_prod_serv,
                        :descripcion,
                        :tipo,
                        :fecha_inicio_vigencia,
                        :fecha_fin_vigencia,
                        :incluir_iva_trasladado,
                        :incluir_ieps_trasladado,
                        :complemento_debe_incluir,
                        :descripcion_clase,
                        :descripcion_grupo,
                        :descripcion_division)
                .add_order(params[:sorted])
                .filter(params[:filtered], FILTERS_WHITE_LIST)

        per_page = pagination_per_page(params[:pageSize])

        paginate items, ::V1::CatalogoProdServSerializer, per_page: per_page
      end
      # rubocop:enable Metrics/MethodLength
    end
  end
end
