module Api
  module V1
    class CategoriasController < ApiController
      DIVISION = 'division'.freeze
      GRUPO = 'grupo'.freeze
      CLASE = 'clase'.freeze

      AVAILABLE_CATEGORIES = [DIVISION, GRUPO, CLASE].freeze
      NOT_FOUND_MESSAGE = 'No encontrado'.freeze

      # TODO: Each category should have a filter white list, like defined in
      # the other controllers. For example:
      # FILTERS_WHITE_LIST = {
      #   DIVISION => %w[tipo_id],
      #   GRUPO => %w[tipo_id division_id],
      #   CLASE => %w[tipo_id grupo_id]
      # }.freeze

      # Might need to implement a mapping of filters to table name, like:
      # { 'division_id' => :division_prod_serv_id }
      # Or mapping to existing scope, like:
      # { 'tipo_id' => :filter_by_tipo }
      # Or just have scopes with the same name as the expected filters, so we can use
      # FilterableModel like in catalogo_prod_servs

      def index
        if AVAILABLE_CATEGORIES.include? category_name
          render json: category_serializer_class.new(
            category_model_class.list_items(params)
          )
        else
          render json: not_found_error_message, status: :not_found
        end
      end

      private

      def not_found_error_message
        { error: NOT_FOUND_MESSAGE }.to_json
      end

      def category_name
        @category_name ||= params[:categoria]
      end

      def category_model_class
        "#{category_name.capitalize}ProdServ".constantize
      end

      def category_serializer_class
        "::V1::#{category_name.capitalize}ProdServSerializer".constantize
      end
    end
  end
end
