module Pagination
  PAGE_SIZE_UPPER_LIMIT = 30
  PAGE_SIZE_LOWER_LIMIT = 1
  PAGE_SIZE_DEFAULT = 20

  def pagination_per_page(page_size)
    return pagination_check_limits(page_size.to_i) if page_size.present?
    PAGE_SIZE_DEFAULT
  end

  private

  def pagination_check_limits(page_size)
    return page_size if page_size.between?(PAGE_SIZE_LOWER_LIMIT, PAGE_SIZE_UPPER_LIMIT)
    PAGE_SIZE_DEFAULT
  end
end
