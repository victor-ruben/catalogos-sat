class CatalogoClaveUnidad < ApplicationRecord
  self.table_name = 'c_clave_unidad'
  include DateScopes
  include SortableModel
  include FilterableModel

  scope :c_clave_unidad, ->(text) { where('c_clave_unidad LIKE ?', text.to_s.upcase) }
  scope :nombre, ->(text) { unaccent_search('nombre', text) }
  scope :descripcion, ->(text) { unaccent_search('descripcion', text) }
  scope :nota, ->(text) { unaccent_search('nota', text) }
  scope :simbolo, ->(text) { unaccent_search('simbolo', text) }
end
