class CatalogoProdServ < ApplicationRecord
  include DateScopes
  include SortableModel
  include FilterableModel

  belongs_to :division_prod_serv
  belongs_to :grupo_prod_serv
  belongs_to :clase_prod_serv

  scope :clave_prod_serv, ->(text) { where('clave_prod_serv LIKE ?', text) }
  scope :descripcion, ->(text) { unaccent_search('descripcion', text) }
  scope :descripcion_clase, ->(text) { unaccent_search('descripcion_clase', text) }
  scope :descripcion_grupo, ->(text) { unaccent_search('descripcion_grupo', text) }
  scope :descripcion_division, ->(text) { unaccent_search('descripcion_division', text) }
  scope :incluir_iva_trasladado, ->(text) { where(incluir_iva_trasladado: text) }
  scope :incluir_ieps_trasladado, ->(text) { where(incluir_ieps_trasladado: text) }
  scope :tipo_id, ->(id) { where(tipo: id) }
  scope :division_id, ->(id) { where(division_prod_serv_id: id) }
  scope :grupo_id, ->(id) { where(grupo_prod_serv_id: id) }
  scope :clase_id, ->(id) { where(clase_prod_serv_id: id) }
end
