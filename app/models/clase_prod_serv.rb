class ClaseProdServ < ApplicationRecord
  include Common

  belongs_to :division_prod_serv
  belongs_to :grupo_prod_serv
  has_many :catalogo_prod_servs
  scope :filter_by_descripcion, lambda { |text|
    where('immutable_unaccent(clase_prod_servs.descripcion) ILIKE immutable_unaccent(?)',
          "%#{text}%")
  }
  scope :filter_by_tipo, ->(id) { where(tipo: id) }
end
