module Common
  extend ActiveSupport::Concern

  PRODUCTO_TIPO_ID = '0'.freeze
  SERVICIO_TIPO_ID = '1'.freeze
  VALID_TIPO_IDS = [PRODUCTO_TIPO_ID, SERVICIO_TIPO_ID].freeze

  class_methods do
    def list_items(filters = nil)
      items = select(:id, :descripcion)
      items = add_scopes(items, filters) if filters.present?
      items
    end

    private

    def add_scopes(items, filters)
      items = add_scope_tipo_id(items, filters['tipo_id'])
      items = add_scope_division_id(items, filters['division_id'])
      items = add_scope_grupo_prod_serv_id(items, filters['grupo_id'])
      items
    end

    def add_scope_tipo_id(items, tipo_id)
      return items.filter_by_tipo(tipo_id) if VALID_TIPO_IDS.include? tipo_id
      items
    end

    def add_scope_division_id(items, division_id)
      division_id ? items.filter_by_division_id(division_id) : items
    end

    def add_scope_grupo_prod_serv_id(items, grupo_id)
      grupo_id ? items.where(grupo_prod_serv_id: grupo_id) : items
    end
  end
end
