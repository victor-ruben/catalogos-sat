module DateScopes
  extend ActiveSupport::Concern

  included do
    scope :fecha_inicio_vigencia, lambda { |date_range|
      filter_date_range('fecha_inicio_vigencia', date_range)
    }
    scope :fecha_fin_vigencia, lambda { |date_range|
      filter_date_range('fecha_fin_vigencia', date_range)
    }
  end
end
