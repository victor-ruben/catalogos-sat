module FilterableModel
  extend ActiveSupport::Concern

  # rubocop:disable Metrics/BlockLength
  class_methods do
    # rubocop:enable Metrics/BlockLength
    # Call the class methods with the same name as the keys in filtering_params
    # with their associated values. Most useful for calling named scopes from
    # URL params. Make sure you don't pass stuff directly from the web without
    # whitelisting only the params you care about first!
    def filter(filtering_params, white_list)
      parsed_params = parse_filters_params(filtering_params).slice(*white_list)
      results = where(nil) # create an anonymous scope
      parsed_params.each do |key, value|
        results = results.public_send(key, value) if filterable?(value)
      end
      results
    end

    def filter_date_range(column_name, date_range)
      date_start = date_range['inicial']
      date_end = date_range['final']

      if date_start.present?
        return where(column_name => date_start..date_end) if date_end.present?
        return where(arel_table[column_name].gteq(date_start))
      elsif date_end.present?
        return where(arel_table[column_name].lteq(date_end))
      end
    end

    # We wrap the column names in immutable_unaccent, is a custom Posgres function used in
    # a trigram index (gin with pg_trgm extension).
    # The right side should be wrapped too, so words with accents are found.
    # Before using this method, check if the column actually have an immutable_unaccent index
    # Usage in model:  scope :whatever, -> (text) { unaccent_search("column_name", text) }
    def unaccent_search(column_name, text)
      where("immutable_unaccent(#{column_name}) ILIKE immutable_unaccent(?)", text)
    end

    private

    def parse_filters_params(params)
      {}.tap { |h| parse_and_add_filters(h, params) if params.present? }
    end

    def parse_and_add_filters(hash, params)
      params[0..10].each do |param|
        param = JSON.parse(param)
        hash.store(param['id'], param['value'])
      end
    end

    def filterable?(value)
      value.present? && add_filter?(value)
    end

    def add_filter?(value)
      value.is_a?(Hash) || value.strip.tr('%', '').length > 0
    end
  end
end
