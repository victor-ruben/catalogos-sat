module SortableModel
  extend ActiveSupport::Concern

  DEFAULT_SORT = { id: :asc }.freeze

  # methods defined here are going to extend the class, not the instance of it
  class_methods do
    def add_order(param)
      # Always order by id asc if not sort specified
      param.blank? ? order(DEFAULT_SORT) : order(sorting(param))
    end

    private

    def sorting(param)
      # Grab first param: Only sorting by one column at a time allowed
      sort = JSON.parse(param.first).symbolize_keys

      return DEFAULT_SORT unless sort[:id]

      order = sort[:desc] ? :desc : :asc

      # TODO: remove when/if tipo column is renamed to tipo_id in the catalogo_prod_servs table
      sort[:id] = 'tipo' if sort[:id] == 'tipo_id'

      { sort[:id].to_sym => order, :id => order }
    end
  end
end
