class DivisionProdServ < ApplicationRecord
  include Common

  has_many :grupo_prod_servs
  has_many :clase_prod_servs
  has_many :catalogo_prod_servs
  scope :filter_by_descripcion, lambda { |text|
    where(DivisionProdServ.arel_table[:descripcion].matches("#{text}%"))
  }
  scope :filter_by_tipo, ->(id) { where(tipo: id) }
end
