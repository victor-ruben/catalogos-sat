class GrupoProdServ < ApplicationRecord
  include Common

  belongs_to :division_prod_serv
  has_many :clase_prod_servs
  has_many :catalogo_prod_servs
  scope :filter_by_descripcion, lambda { |text|
    where(GrupoProdServ.arel_table[:descripcion].matches("#{text}%"))
  }
  scope :filter_by_tipo, ->(id) { where(tipo: id) }
  scope :filter_by_division_id, ->(id) { where(division_prod_serv_id: id) }
end
