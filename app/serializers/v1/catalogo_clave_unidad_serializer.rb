module V1
  class CatalogoClaveUnidadSerializer
    include FastJsonapi::ObjectSerializer

    set_type :catalogo_clave_unidad
    attributes :c_clave_unidad,
               :nombre,
               :descripcion,
               :nota,
               :fecha_inicio_vigencia,
               :fecha_fin_vigencia,
               :simbolo
  end
end
