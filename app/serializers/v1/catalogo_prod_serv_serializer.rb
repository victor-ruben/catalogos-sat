module V1
  class CatalogoProdServSerializer
    include FastJsonapi::ObjectSerializer

    attributes :clave_prod_serv,
               :descripcion,
               :tipo,
               :descripcion_clase,
               :descripcion_grupo,
               :descripcion_division,
               :fecha_inicio_vigencia,
               :fecha_fin_vigencia,
               :incluir_iva_trasladado,
               :incluir_ieps_trasladado,
               :complemento_debe_incluir
  end
end
