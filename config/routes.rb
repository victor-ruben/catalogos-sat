ERROR_404 = proc { [404, {}, ['']] }

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: ERROR_404

  scope module: :api do
    namespace :v1 do
      resources :catalogo_prod_servs, only: [:index, :show]
      get 'categoria/:categoria', to: 'categorias#index'
      resources :catalogo_clave_unidad, only: [:index]
    end
  end

  # Respond with empty 404 for any other route
  match '*path', via: :all, to: ERROR_404
end
