class CreateCatalogoProdServs < ActiveRecord::Migration[5.1]
  def change
    create_table :catalogo_prod_servs do |t|
      t.string :clave_prod_serv, null: false
      t.string :descripcion, null: false
      t.date   :fecha_inicio_vigencia, null: false
      t.date   :fecha_fin_vigencia
      t.string :incluir_iva_trasladado
      t.string :incluir_ieps_trasladado
      t.string :complemento_debe_incluir

      t.integer :tipo
      t.integer :division_prod_serv_id
      t.integer :grupo_prod_serv_id
      t.integer :clase_prod_serv_id

      t.timestamps
    end
    add_index :catalogo_prod_servs, :clave_prod_serv, unique: true
    add_index :catalogo_prod_servs, :fecha_inicio_vigencia
    add_index :catalogo_prod_servs, :fecha_fin_vigencia

    add_index :catalogo_prod_servs, :tipo
    add_index :catalogo_prod_servs, :division_prod_serv_id
    add_index :catalogo_prod_servs, :grupo_prod_serv_id
    add_index :catalogo_prod_servs, :clase_prod_serv_id
  end
end
