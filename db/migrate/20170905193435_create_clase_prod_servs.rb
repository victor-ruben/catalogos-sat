class CreateClaseProdServs < ActiveRecord::Migration[5.1]
  def change
    create_table :clase_prod_servs do |t|
      t.string :clave_clase, null: false
      t.string :descripcion, null: false
      t.integer :tipo

      t.timestamps
    end
    add_index :clase_prod_servs, :clave_clase, unique: true
    add_index :clase_prod_servs, :tipo
  end
end
