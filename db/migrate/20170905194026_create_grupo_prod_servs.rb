class CreateGrupoProdServs < ActiveRecord::Migration[5.1]
  def change
    create_table :grupo_prod_servs do |t|
      t.string :clave_grupo, null: false
      t.string :descripcion, null: false
      t.integer :tipo

      t.timestamps
    end
    add_index :grupo_prod_servs, :clave_grupo, unique: true
    add_index :grupo_prod_servs, :tipo
  end
end
