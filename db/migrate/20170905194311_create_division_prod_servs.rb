class CreateDivisionProdServs < ActiveRecord::Migration[5.1]
  def change
    create_table :division_prod_servs do |t|
      t.string :clave_division, null: false
      t.string :descripcion, null: false
      t.integer :tipo

      t.timestamps
    end
    add_index :division_prod_servs, :clave_division, unique: true
    add_index :division_prod_servs, :tipo
  end
end
