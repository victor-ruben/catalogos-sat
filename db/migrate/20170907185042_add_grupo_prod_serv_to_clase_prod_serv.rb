class AddGrupoProdServToClaseProdServ < ActiveRecord::Migration[5.1]
  def change
    add_reference :clase_prod_servs, :grupo_prod_serv, foreign_key: false
  end
end
