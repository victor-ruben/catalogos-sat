class AddDivisionProdServToClaseProdServ < ActiveRecord::Migration[5.1]
  def change
    add_reference :clase_prod_servs, :division_prod_serv, foreign_key: false
  end
end
