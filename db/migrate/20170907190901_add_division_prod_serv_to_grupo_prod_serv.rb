class AddDivisionProdServToGrupoProdServ < ActiveRecord::Migration[5.1]
  def change
    add_reference :grupo_prod_servs, :division_prod_serv, foreign_key: false
  end
end
