class AddIndexOnCatalogoProdServDescripcion < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    # In order to use the pg_trgm and unaccent extensions in an index, we need to wrap the unaccent function
    # and mark it as IMMUTABLE
    execute <<-SQL
    CREATE OR REPLACE FUNCTION immutable_unaccent(text)
      RETURNS text AS
      $func$
      SELECT public.unaccent('public.unaccent', $1)  -- schema-qualify function and dictionary
      $func$  LANGUAGE sql IMMUTABLE;
    SQL

    # Then we create an index with gin_trgm and apply the function created above
    # Now we can have fast search queries with ILIKE and LIKE but they should wrap the column like this:
    # CatalogoProdServ.where("immutable_unaccent(catalogo_prod_servs.descripcion) ILIKE ?", "%#{query}%")
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_gin_idx ON catalogo_prod_servs USING gin(immutable_unaccent(descripcion) gin_trgm_ops);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX catalogo_prod_servs_descripcion_gin_idx;
    DROP FUNCTION IF EXISTS immutable_unaccent(text);
    SQL
  end
end
