class AddIndexOnCatalogoProdServClaveProdServ < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    # Now we can have fast search queries with ILIKE and LIKE but they should wrap the column like this:
    # CatalogoProdServ.where("immutable_unaccent(catalogo_prod_servs.clave_prod_serv) ILIKE ?", "%#{query}%")
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_clave_prod_serv_gin_idx ON catalogo_prod_servs USING gin(clave_prod_serv gin_trgm_ops);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX catalogo_prod_servs_clave_prod_serv_gin_idx;
    SQL
  end
end
