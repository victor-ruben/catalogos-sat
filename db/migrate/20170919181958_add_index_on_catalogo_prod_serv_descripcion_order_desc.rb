class AddIndexOnCatalogoProdServDescripcionOrderDesc < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_order_desc_idx ON catalogo_prod_servs (descripcion DESC);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX catalogo_prod_servs_descripcion_order_desc_idx;
    SQL
  end
end
