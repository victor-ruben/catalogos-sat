class AddIndexOnClaseProdServDescripcion < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    execute <<-SQL
    CREATE INDEX CONCURRENTLY clase_prod_servs_descripcion_gin_idx ON clase_prod_servs USING gin(immutable_unaccent(descripcion) gin_trgm_ops);
    SQL

    execute <<-SQL
    CREATE INDEX CONCURRENTLY clase_prod_servs_descripcion_desc ON clase_prod_servs (descripcion DESC);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX clase_prod_servs_descripcion_gin_idx;
    DROP INDEX clase_prod_servs_descripcion_desc;
    SQL
  end
end
