class AddDescripcionesColumnsToCatalogoProdServ < ActiveRecord::Migration[5.1]
  def change
    add_column :catalogo_prod_servs, :descripcion_clase, :string
    add_column :catalogo_prod_servs, :descripcion_grupo, :string
    add_column :catalogo_prod_servs, :descripcion_division, :string
  end
end
