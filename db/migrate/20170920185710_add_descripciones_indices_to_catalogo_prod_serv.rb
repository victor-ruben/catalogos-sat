class AddDescripcionesIndicesToCatalogoProdServ < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_clase_gin_idx ON catalogo_prod_servs USING gin(immutable_unaccent(descripcion_clase) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_grupo_gin_idx ON catalogo_prod_servs USING gin(immutable_unaccent(descripcion_grupo) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_division_gin_idx ON catalogo_prod_servs USING gin(immutable_unaccent(descripcion_division) gin_trgm_ops);
    SQL

    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_clase_desc ON catalogo_prod_servs (descripcion_clase DESC);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_grupo_desc ON catalogo_prod_servs (descripcion_grupo DESC);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_division_desc ON catalogo_prod_servs (descripcion_division DESC);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX catalogo_prod_servs_descripcion_clase_gin_idx;
    DROP INDEX catalogo_prod_servs_descripcion_grupo_gin_idx;
    DROP INDEX catalogo_prod_servs_descripcion_division_gin_idx;
    DROP INDEX catalogo_prod_servs_descripcion_clase_desc;
    DROP INDEX catalogo_prod_servs_descripcion_grupo_desc;
    DROP INDEX catalogo_prod_servs_descripcion_division_desc;
    SQL
  end
end
