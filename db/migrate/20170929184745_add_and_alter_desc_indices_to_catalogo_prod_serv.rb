class AddAndAlterDescIndicesToCatalogoProdServ < ActiveRecord::Migration[5.1]
  disable_ddl_transaction!

  def up
    execute <<-SQL
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_clase_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_grupo_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_division_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_order_desc_idx;
    DROP INDEX IF EXISTS catalogo_prod_servs_clave_prod_serv_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_tipo_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_fecha_inicio_vigencia_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_fecha_fin_vigencia_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_incluir_iva_trasladado_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_incluir_ieps_trasladado_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_complemento_debe_incluir_desc;
    SQL

    # Old updated (created with same name) with appended id DESC
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_clase_desc ON catalogo_prod_servs (descripcion_clase DESC, id DESC);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_grupo_desc ON catalogo_prod_servs (descripcion_grupo DESC, id DESC);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_division_desc ON catalogo_prod_servs (descripcion_division DESC, id DESC);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_descripcion_order_desc_idx ON catalogo_prod_servs (descripcion DESC, id DESC);
    SQL
    # New indices
    # clave_prod_serv
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_clave_prod_serv_desc ON catalogo_prod_servs (clave_prod_serv DESC, id DESC);
    SQL
    # tipo
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_tipo_desc ON catalogo_prod_servs (tipo DESC, id DESC);
    SQL
    # fecha_inicio_vigencia
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_fecha_inicio_vigencia_desc ON catalogo_prod_servs (fecha_inicio_vigencia DESC, id DESC);
    SQL
    # fecha_fin_vigencia
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_fecha_fin_vigencia_desc ON catalogo_prod_servs (fecha_fin_vigencia DESC, id DESC);
    SQL
    # incluir_iva_trasladado
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_incluir_iva_trasladado_desc ON catalogo_prod_servs (incluir_iva_trasladado DESC, id DESC);
    SQL
    # incluir_ieps_trasladado
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_incluir_ieps_trasladado_desc ON catalogo_prod_servs (incluir_ieps_trasladado DESC, id DESC);
    SQL
    # complemento_debe_incluir
    execute <<-SQL
    CREATE INDEX CONCURRENTLY catalogo_prod_servs_complemento_debe_incluir_desc ON catalogo_prod_servs (complemento_debe_incluir DESC, id DESC);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_clase_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_grupo_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_division_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_descripcion_order_desc_idx;
    DROP INDEX IF EXISTS catalogo_prod_servs_clave_prod_serv_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_tipo_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_fecha_inicio_vigencia_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_fecha_fin_vigencia_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_incluir_iva_trasladado_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_incluir_ieps_trasladado_desc;
    DROP INDEX IF EXISTS catalogo_prod_servs_complemento_debe_incluir_desc;
    SQL
  end
end
