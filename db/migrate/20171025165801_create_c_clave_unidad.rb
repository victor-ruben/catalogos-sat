class CreateCClaveUnidad < ActiveRecord::Migration[5.1]
  def change
    create_table :c_clave_unidad do |t|
      t.string :c_clave_unidad, null: false
      t.string :nombre, null: false
      t.string :descripcion
      t.string :nota
      t.date   :fecha_inicio_vigencia
      t.date   :fecha_fin_vigencia
      t.string :simbolo
      t.timestamps
    end
  end
end
