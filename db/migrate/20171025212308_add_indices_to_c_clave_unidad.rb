class AddIndicesToCClaveUnidad < ActiveRecord::Migration[5.1]
  # An index can be created concurrently only outside of a transaction.
  disable_ddl_transaction!

  def up
    # c_clave_unidad
    execute <<-SQL
    CREATE UNIQUE INDEX CONCURRENTLY c_clave_unidad_c_clave_unidad_key ON c_clave_unidad (c_clave_unidad);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_c_clave_unidad_gin_idx ON c_clave_unidad USING gin(c_clave_unidad gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_c_clave_unidad_desc_idx ON c_clave_unidad (c_clave_unidad DESC, id DESC);
    SQL
    # nombre
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_nombre_gin_idx ON c_clave_unidad USING gin(immutable_unaccent(nombre) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_nombre_desc_idx ON c_clave_unidad (nombre DESC, id DESC);
    SQL
    # descripcion
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_descripcion_gin_idx ON c_clave_unidad USING gin(immutable_unaccent(descripcion) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_descripcion_desc_idx ON c_clave_unidad (descripcion DESC, id DESC);
    SQL
    # nota
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_nota_gin_idx ON c_clave_unidad USING gin(immutable_unaccent(nota) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_nota_desc_idx ON c_clave_unidad (nota DESC, id DESC);
    SQL
    # fecha_inicio_vigencia
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_fecha_inicio_vigencia_idx ON c_clave_unidad (fecha_inicio_vigencia);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_fecha_inicio_vigencia_desc_idx ON c_clave_unidad (fecha_inicio_vigencia DESC, id DESC);
    SQL
    # fecha_fin_vigencia
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_fecha_fin_vigencia_idx ON c_clave_unidad (fecha_fin_vigencia);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_fecha_fin_vigencia_desc_idx ON c_clave_unidad (fecha_fin_vigencia DESC, id DESC);
    SQL
    # simbolo
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_simbolo_gin_idx ON c_clave_unidad USING gin(immutable_unaccent(simbolo) gin_trgm_ops);
    SQL
    execute <<-SQL
    CREATE INDEX CONCURRENTLY c_clave_unidad_simbolo_desc_idx ON c_clave_unidad (simbolo DESC, id DESC);
    SQL
  end

  def down
    execute <<-SQL
    DROP INDEX IF EXISTS c_clave_unidad_c_clave_unidad_key;
    DROP INDEX IF EXISTS c_clave_unidad_c_clave_unidad_gin_idx;
    DROP INDEX IF EXISTS c_clave_unidad_c_clave_unidad_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_nombre_gin_idx;
    DROP INDEX IF EXISTS c_clave_unidad_nombre_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_descripcion_gin_idx;
    DROP INDEX IF EXISTS c_clave_unidad_descripcion_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_nota_gin_idx;
    DROP INDEX IF EXISTS c_clave_unidad_nota_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_fecha_inicio_vigencia_idx;
    DROP INDEX IF EXISTS c_clave_unidad_fecha_inicio_vigencia_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_fecha_fin_vigencia_idx;
    DROP INDEX IF EXISTS c_clave_unidad_fecha_fin_vigencia_desc_idx;

    DROP INDEX IF EXISTS c_clave_unidad_simbolo_gin_idx;
    DROP INDEX IF EXISTS c_clave_unidad_simbolo_desc_idx;
    SQL
  end
end
