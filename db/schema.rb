# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170918172927) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"

  create_table "catalogo_prod_servs", force: :cascade do |t|
    t.string "clave_prod_serv", null: false
    t.string "descripcion", null: false
    t.date "fecha_inicio_vigencia", null: false
    t.date "fecha_fin_vigencia"
    t.string "incluir_iva_trasladado"
    t.string "incluir_ieps_trasladado"
    t.string "complemento_debe_incluir"
    t.integer "tipo"
    t.integer "division_prod_serv_id"
    t.integer "grupo_prod_serv_id"
    t.integer "clase_prod_serv_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clase_prod_serv_id"], name: "index_catalogo_prod_servs_on_clase_prod_serv_id"
    t.index ["clave_prod_serv"], name: "index_catalogo_prod_servs_on_clave_prod_serv", unique: true
    t.index ["division_prod_serv_id"], name: "index_catalogo_prod_servs_on_division_prod_serv_id"
    t.index ["fecha_fin_vigencia"], name: "index_catalogo_prod_servs_on_fecha_fin_vigencia"
    t.index ["fecha_inicio_vigencia"], name: "index_catalogo_prod_servs_on_fecha_inicio_vigencia"
    t.index ["grupo_prod_serv_id"], name: "index_catalogo_prod_servs_on_grupo_prod_serv_id"
    t.index ["tipo"], name: "index_catalogo_prod_servs_on_tipo"
  end

  create_table "clase_prod_servs", force: :cascade do |t|
    t.string "clave_clase", null: false
    t.string "descripcion", null: false
    t.integer "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "grupo_prod_serv_id"
    t.bigint "division_prod_serv_id"
    t.index ["clave_clase"], name: "index_clase_prod_servs_on_clave_clase", unique: true
    t.index ["division_prod_serv_id"], name: "index_clase_prod_servs_on_division_prod_serv_id"
    t.index ["grupo_prod_serv_id"], name: "index_clase_prod_servs_on_grupo_prod_serv_id"
    t.index ["tipo"], name: "index_clase_prod_servs_on_tipo"
  end

  create_table "division_prod_servs", force: :cascade do |t|
    t.string "clave_division", null: false
    t.string "descripcion", null: false
    t.integer "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["clave_division"], name: "index_division_prod_servs_on_clave_division", unique: true
    t.index ["tipo"], name: "index_division_prod_servs_on_tipo"
  end

  create_table "grupo_prod_servs", force: :cascade do |t|
    t.string "clave_grupo", null: false
    t.string "descripcion", null: false
    t.integer "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "division_prod_serv_id"
    t.index ["clave_grupo"], name: "index_grupo_prod_servs_on_clave_grupo", unique: true
    t.index ["division_prod_serv_id"], name: "index_grupo_prod_servs_on_division_prod_serv_id"
    t.index ["tipo"], name: "index_grupo_prod_servs_on_tipo"
  end
end
