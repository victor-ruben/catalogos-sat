# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#

# Fill catalogo_prod_servs table with values of the official SAT catalog (only the c_ClaveProdServ sheet) downloaded on 05 Sep 2017
def seed_catalogo_prod_serv
  require 'creek'
  creek = Creek::Book.new 'db/c_ClaveProdServ.xlsx'
  sheet = creek.sheets[0]

  header = nil
  sheet.rows_with_meta_data.each do |row|
    header = row['cells'].values
    break
  end

  header.map!(&:parameterize)

  sheet.rows_with_meta_data.each do |row|
    next if row['r'].to_i <= 1
    r = Hash[header.zip(row['cells'].values)]

    CatalogoProdServ.new.tap do |ps|
      ps.clave_prod_serv = r['c_claveprodserv'].to_s.gsub(/\.0/, '') # To avoid .0 at the end like "10101010.0"
      ps.descripcion = r['descripcion']
      ps.fecha_inicio_vigencia = r['fechainiciovigencia']
      ps.fecha_fin_vigencia = r['fechafinvigencia']
      ps.incluir_iva_trasladado = r['incluir-iva-trasladado']
      ps.incluir_ieps_trasladado = r['incluir-ieps-trasladado']
      ps.complemento_debe_incluir = r['complemento-que-debe-incluir']

      ps.save!
    end
  end
end

# Fill "Division, Grupo ans Clase" prod_servs tables via web scraping the official (but really crappy) online search tool
def seed_division_grupo_clase_prod_serv
  # watir requires you have installed a browser driver and have it available in you $PATH
  # I am using chromedriver 64x on ubuntu 16.04
  require 'watir'

  # Id's of select lists
  # cmbTipo
  # cmbSegmento
  # cmbFamilia
  # cmbClase

  b = Watir::Browser.start URL, :chrome, headless: true
  divisiones = {} # cmbSegmento
  grupos = {} # cmbFamilia
  clases = {} # cmbClase

  s_tipo = b.select_list id: 'cmbTipo'
  s_segmento = b.select_list id: 'cmbSegmento'
  s_grupo = b.select_list id: 'cmbFamilia'
  s_clase = b.select_list id: 'cmbClase'

  %w[Productos Servicios].each do |cmb_tipo|
    puts '-- BEGINNING TIPO: ' + cmb_tipo
    # 0 = Productos, 1 = Servicios
    tipo = cmb_tipo == 'Productos' ? 0 : 1
    s_tipo.select cmb_tipo

    divisiones = Hash[ Nokogiri::HTML.fragment(s_segmento.html).css('option').map { |o| [o['value'], o.text] } ]

    divisiones.each_with_index do |(key, value), idx|
      next if key == '0'

      DivisionProdServ.new.tap do |d|
        d.clave_division = key
        d.descripcion = value
        d.tipo = tipo
        d.save!
      end

      s_segmento.select key
      grupos = Hash[ Nokogiri::HTML.fragment(s_grupo.html).css('option').map { |o| [o['value'], o.text] } ]

      grupos.each_with_index do |(key, value), idx|
        next if key == '0'

        GrupoProdServ.new.tap do |d|
          d.clave_grupo = key
          d.descripcion = value
          d.tipo = tipo
          d.save!
        end

        s_grupo.select key
        clases = Hash[ Nokogiri::HTML.fragment(s_clase.html).css('option').map { |o| [o['value'], o.text] } ]

        clases.each do |key, value|
          next if key == '0'

          ClaseProdServ.new.tap do |d|
            d.clave_clase = key
            d.descripcion = value
            d.tipo = tipo
            d.save!
          end
        end
      end
      puts 'FINISHED DIVISION: ' + value
    end
    puts '-- FINISHED TIPO: ' + cmb_tipo
  end
end

# Fill "Division, Grupo ans Clase" ids columns in catalogo_prod_servs
def fill_catalogo_prod_serv_ids
  DivisionProdServ.all.each do |d|
    GrupoProdServ.where('clave_grupo like ?', "#{d.clave_division}%").update_all(division_prod_serv_id: d.id)
    ClaseProdServ.where('clave_clase like ?', "#{d.clave_division}%").update_all(division_prod_serv_id: d.id)
    CatalogoProdServ.where('clave_prod_serv like ?', "#{d.clave_division}%").update_all(division_prod_serv_id: d.id, tipo: d.tipo)
  end

  GrupoProdServ.all.each do |d|
    ClaseProdServ.where('clave_clase like ?', "#{d.clave_grupo}%").update_all(grupo_prod_serv_id: d.id)
    CatalogoProdServ.where('clave_prod_serv like ?', "#{d.clave_grupo}%").update_all(grupo_prod_serv_id: d.id)
  end

  ClaseProdServ.all.each do |d|
    CatalogoProdServ.where('clave_prod_serv like ?', "#{d.clave_clase}%").update_all(clase_prod_serv_id: d.id)
  end
end

# Fill the descripcion_clase, descripcion_grupo and descripcion_division columns
# Loop all CatalogoProdServ cause we need to save '' for records that don't have an
# associated id's of Clase, Grupo or Division
def fill_catalogo_prod_serv_descripciones
  CatalogoProdServ.all.each do |prod|
    desc_clase = ClaseProdServ.where(id: prod.clase_prod_serv_id).pluck(:descripcion).first
    prod.descripcion_clase = desc_clase ? desc_clase : ''

    desc_grupo = GrupoProdServ.where(id: prod.grupo_prod_serv_id).pluck(:descripcion).first
    prod.descripcion_grupo = desc_grupo ? desc_grupo : ''

    desc_division = DivisionProdServ.where(id: prod.division_prod_serv_id).pluck(:descripcion).first
    prod.descripcion_division = desc_division ? desc_division : ''

    prod.save!(validate: false)
  end
end

# Fill c_clave_unidad table with values of the official SAT catalog (only the c_ClaveUnidad sheet) downloaded on 25 oct 2017
def seed_catalogo_clave_unidad
  require 'creek'
  creek = Creek::Book.new 'db/c_ClaveUnidad.xlsx'
  sheet = creek.sheets[0]

  header = nil
  sheet.rows_with_meta_data.each do |row|
    header = row['cells'].values
    break
  end

  header.compact! # Delete nil header columns from array
  header.map!(&:parameterize)

  sheet.rows_with_meta_data.each do |row|
    next if row['r'].to_i <= 1
    r = Hash[header.zip(row['cells'].values)]

    CatalogoClaveUnidad.new.tap do |ps|
      ps.c_clave_unidad = r['c_claveunidad'].to_s.gsub(/\.0/, '') # To avoid .0 at the end like "10101010.0"
      ps.nombre = r['nombre']
      ps.descripcion = r['descripcion']
      ps.nota = r['nota']
      ps.fecha_inicio_vigencia = r['fecha-de-inicio-de-vigencia']
      ps.fecha_fin_vigencia = r['fecha-de-fin-de-vigencia']
      ps.simbolo = r['simbolo']
      ps.save!
    end
  end
end

def determine_new_and_deleted_records
  require 'creek'
  creek = Creek::Book.new 'db/c_ClaveProdServ_22-Nov-2017.xlsx'
  sheet = creek.sheets[0]

  header = nil
  sheet.rows_with_meta_data.each do |row|
    header = row['cells'].values
    break
  end

  header.map!(&:parameterize)

  # 8 nuevos
  # 331 eliminados
  registros = 0
  set_claves = Set.new
  puts 'REGISTROS NUEVOS:'
  sheet.rows_with_meta_data.each do |row|
    next if row['r'].to_i <= 1
    r = Hash[header.zip(row['cells'].values)]

    c_clave_prodserv = r['c_claveprodserv'].to_s.gsub(/\.0/, '')
    registros += 1 if c_clave_prodserv.present?

    producto = CatalogoProdServ.select(:id).where(clave_prod_serv: c_clave_prodserv)

    if c_clave_prodserv.present?
      set_claves.add(c_clave_prodserv)
      puts "'#{c_clave_prodserv}'," if producto.blank?
    end
  end

  puts 'REGISTROS QUE SE ELIMINAN:'
  CatalogoProdServ.pluck(:clave_prod_serv).each do |clave_prod_serv|
    puts "'#{clave_prod_serv}'," unless set_claves.include? clave_prod_serv
  end

  puts "Numero de registros: #{registros}"
end

# Call methods
URL = 'http://200.57.3.46:443/PyS/catPyS.aspx'.freeze
# seed_catalogo_prod_serv()
# seed_division_grupo_clase_prod_serv()
# fill_catalogo_prod_serv_ids()
# fill_catalogo_prod_serv_descripciones()
# seed_catalogo_clave_unidad()
# determine_new_and_deleted_records()
