SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


--
-- Name: unaccent; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS unaccent WITH SCHEMA public;


--
-- Name: EXTENSION unaccent; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION unaccent IS 'text search dictionary that removes accents';


SET search_path = public, pg_catalog;

--
-- Name: immutable_unaccent(text); Type: FUNCTION; Schema: public; Owner: -
--

CREATE FUNCTION immutable_unaccent(text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
      SELECT public.unaccent('public.unaccent', $1)  -- schema-qualify function and dictionary
      $_$;


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: ar_internal_metadata; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE ar_internal_metadata (
    key character varying NOT NULL,
    value character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: c_clave_unidad; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE c_clave_unidad (
    id bigint NOT NULL,
    c_clave_unidad character varying NOT NULL,
    nombre character varying NOT NULL,
    descripcion character varying,
    nota character varying,
    fecha_inicio_vigencia date,
    fecha_fin_vigencia date,
    simbolo character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: c_clave_unidad_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE c_clave_unidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: c_clave_unidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE c_clave_unidad_id_seq OWNED BY c_clave_unidad.id;


--
-- Name: catalogo_prod_servs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE catalogo_prod_servs (
    id bigint NOT NULL,
    clave_prod_serv character varying NOT NULL,
    descripcion character varying NOT NULL,
    fecha_inicio_vigencia date NOT NULL,
    fecha_fin_vigencia date,
    incluir_iva_trasladado character varying,
    incluir_ieps_trasladado character varying,
    complemento_debe_incluir character varying,
    tipo integer,
    division_prod_serv_id integer,
    grupo_prod_serv_id integer,
    clase_prod_serv_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    descripcion_clase character varying,
    descripcion_grupo character varying,
    descripcion_division character varying
);


--
-- Name: catalogo_prod_servs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE catalogo_prod_servs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: catalogo_prod_servs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE catalogo_prod_servs_id_seq OWNED BY catalogo_prod_servs.id;


--
-- Name: clase_prod_servs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE clase_prod_servs (
    id bigint NOT NULL,
    clave_clase character varying NOT NULL,
    descripcion character varying NOT NULL,
    tipo integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    grupo_prod_serv_id bigint,
    division_prod_serv_id bigint
);


--
-- Name: clase_prod_servs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE clase_prod_servs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clase_prod_servs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE clase_prod_servs_id_seq OWNED BY clase_prod_servs.id;


--
-- Name: division_prod_servs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE division_prod_servs (
    id bigint NOT NULL,
    clave_division character varying NOT NULL,
    descripcion character varying NOT NULL,
    tipo integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


--
-- Name: division_prod_servs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE division_prod_servs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: division_prod_servs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE division_prod_servs_id_seq OWNED BY division_prod_servs.id;


--
-- Name: grupo_prod_servs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE grupo_prod_servs (
    id bigint NOT NULL,
    clave_grupo character varying NOT NULL,
    descripcion character varying NOT NULL,
    tipo integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL,
    division_prod_serv_id bigint
);


--
-- Name: grupo_prod_servs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE grupo_prod_servs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: grupo_prod_servs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE grupo_prod_servs_id_seq OWNED BY grupo_prod_servs.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


--
-- Name: c_clave_unidad id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY c_clave_unidad ALTER COLUMN id SET DEFAULT nextval('c_clave_unidad_id_seq'::regclass);


--
-- Name: catalogo_prod_servs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY catalogo_prod_servs ALTER COLUMN id SET DEFAULT nextval('catalogo_prod_servs_id_seq'::regclass);


--
-- Name: clase_prod_servs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY clase_prod_servs ALTER COLUMN id SET DEFAULT nextval('clase_prod_servs_id_seq'::regclass);


--
-- Name: division_prod_servs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY division_prod_servs ALTER COLUMN id SET DEFAULT nextval('division_prod_servs_id_seq'::regclass);


--
-- Name: grupo_prod_servs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY grupo_prod_servs ALTER COLUMN id SET DEFAULT nextval('grupo_prod_servs_id_seq'::regclass);


--
-- Name: ar_internal_metadata ar_internal_metadata_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY ar_internal_metadata
    ADD CONSTRAINT ar_internal_metadata_pkey PRIMARY KEY (key);


--
-- Name: c_clave_unidad c_clave_unidad_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY c_clave_unidad
    ADD CONSTRAINT c_clave_unidad_pkey PRIMARY KEY (id);


--
-- Name: catalogo_prod_servs catalogo_prod_servs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY catalogo_prod_servs
    ADD CONSTRAINT catalogo_prod_servs_pkey PRIMARY KEY (id);


--
-- Name: clase_prod_servs clase_prod_servs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY clase_prod_servs
    ADD CONSTRAINT clase_prod_servs_pkey PRIMARY KEY (id);


--
-- Name: division_prod_servs division_prod_servs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY division_prod_servs
    ADD CONSTRAINT division_prod_servs_pkey PRIMARY KEY (id);


--
-- Name: grupo_prod_servs grupo_prod_servs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY grupo_prod_servs
    ADD CONSTRAINT grupo_prod_servs_pkey PRIMARY KEY (id);


--
-- Name: schema_migrations schema_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY schema_migrations
    ADD CONSTRAINT schema_migrations_pkey PRIMARY KEY (version);


--
-- Name: c_clave_unidad_c_clave_unidad_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_c_clave_unidad_desc_idx ON c_clave_unidad USING btree (c_clave_unidad DESC, id DESC);


--
-- Name: c_clave_unidad_c_clave_unidad_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_c_clave_unidad_gin_idx ON c_clave_unidad USING gin (c_clave_unidad gin_trgm_ops);


--
-- Name: c_clave_unidad_c_clave_unidad_key; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX c_clave_unidad_c_clave_unidad_key ON c_clave_unidad USING btree (c_clave_unidad);


--
-- Name: c_clave_unidad_descripcion_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_descripcion_desc_idx ON c_clave_unidad USING btree (descripcion DESC, id DESC);


--
-- Name: c_clave_unidad_descripcion_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_descripcion_gin_idx ON c_clave_unidad USING gin (immutable_unaccent((descripcion)::text) gin_trgm_ops);


--
-- Name: c_clave_unidad_fecha_fin_vigencia_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_fecha_fin_vigencia_desc_idx ON c_clave_unidad USING btree (fecha_fin_vigencia DESC, id DESC);


--
-- Name: c_clave_unidad_fecha_fin_vigencia_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_fecha_fin_vigencia_idx ON c_clave_unidad USING btree (fecha_fin_vigencia);


--
-- Name: c_clave_unidad_fecha_inicio_vigencia_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_fecha_inicio_vigencia_desc_idx ON c_clave_unidad USING btree (fecha_inicio_vigencia DESC, id DESC);


--
-- Name: c_clave_unidad_fecha_inicio_vigencia_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_fecha_inicio_vigencia_idx ON c_clave_unidad USING btree (fecha_inicio_vigencia);


--
-- Name: c_clave_unidad_nombre_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_nombre_desc_idx ON c_clave_unidad USING btree (nombre DESC, id DESC);


--
-- Name: c_clave_unidad_nombre_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_nombre_gin_idx ON c_clave_unidad USING gin (immutable_unaccent((nombre)::text) gin_trgm_ops);


--
-- Name: c_clave_unidad_nota_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_nota_desc_idx ON c_clave_unidad USING btree (nota DESC, id DESC);


--
-- Name: c_clave_unidad_nota_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_nota_gin_idx ON c_clave_unidad USING gin (immutable_unaccent((nota)::text) gin_trgm_ops);


--
-- Name: c_clave_unidad_simbolo_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_simbolo_desc_idx ON c_clave_unidad USING btree (simbolo DESC, id DESC);


--
-- Name: c_clave_unidad_simbolo_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX c_clave_unidad_simbolo_gin_idx ON c_clave_unidad USING gin (immutable_unaccent((simbolo)::text) gin_trgm_ops);


--
-- Name: catalogo_prod_servs_clave_prod_serv_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_clave_prod_serv_desc ON catalogo_prod_servs USING btree (clave_prod_serv DESC, id DESC);


--
-- Name: catalogo_prod_servs_clave_prod_serv_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_clave_prod_serv_gin_idx ON catalogo_prod_servs USING gin (clave_prod_serv gin_trgm_ops);


--
-- Name: catalogo_prod_servs_complemento_debe_incluir_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_complemento_debe_incluir_desc ON catalogo_prod_servs USING btree (complemento_debe_incluir DESC, id DESC);


--
-- Name: catalogo_prod_servs_descripcion_clase_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_clase_desc ON catalogo_prod_servs USING btree (descripcion_clase DESC, id DESC);


--
-- Name: catalogo_prod_servs_descripcion_clase_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_clase_gin_idx ON catalogo_prod_servs USING gin (immutable_unaccent((descripcion_clase)::text) gin_trgm_ops);


--
-- Name: catalogo_prod_servs_descripcion_division_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_division_desc ON catalogo_prod_servs USING btree (descripcion_division DESC, id DESC);


--
-- Name: catalogo_prod_servs_descripcion_division_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_division_gin_idx ON catalogo_prod_servs USING gin (immutable_unaccent((descripcion_division)::text) gin_trgm_ops);


--
-- Name: catalogo_prod_servs_descripcion_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_gin_idx ON catalogo_prod_servs USING gin (immutable_unaccent((descripcion)::text) gin_trgm_ops);


--
-- Name: catalogo_prod_servs_descripcion_grupo_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_grupo_desc ON catalogo_prod_servs USING btree (descripcion_grupo DESC, id DESC);


--
-- Name: catalogo_prod_servs_descripcion_grupo_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_grupo_gin_idx ON catalogo_prod_servs USING gin (immutable_unaccent((descripcion_grupo)::text) gin_trgm_ops);


--
-- Name: catalogo_prod_servs_descripcion_order_desc_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_descripcion_order_desc_idx ON catalogo_prod_servs USING btree (descripcion DESC, id DESC);


--
-- Name: catalogo_prod_servs_fecha_fin_vigencia_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_fecha_fin_vigencia_desc ON catalogo_prod_servs USING btree (fecha_fin_vigencia DESC, id DESC);


--
-- Name: catalogo_prod_servs_fecha_inicio_vigencia_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_fecha_inicio_vigencia_desc ON catalogo_prod_servs USING btree (fecha_inicio_vigencia DESC, id DESC);


--
-- Name: catalogo_prod_servs_incluir_ieps_trasladado_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_incluir_ieps_trasladado_desc ON catalogo_prod_servs USING btree (incluir_ieps_trasladado DESC, id DESC);


--
-- Name: catalogo_prod_servs_incluir_iva_trasladado_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_incluir_iva_trasladado_desc ON catalogo_prod_servs USING btree (incluir_iva_trasladado DESC, id DESC);


--
-- Name: catalogo_prod_servs_tipo_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX catalogo_prod_servs_tipo_desc ON catalogo_prod_servs USING btree (tipo DESC, id DESC);


--
-- Name: clase_prod_servs_descripcion_desc; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX clase_prod_servs_descripcion_desc ON clase_prod_servs USING btree (descripcion DESC);


--
-- Name: clase_prod_servs_descripcion_gin_idx; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX clase_prod_servs_descripcion_gin_idx ON clase_prod_servs USING gin (immutable_unaccent((descripcion)::text) gin_trgm_ops);


--
-- Name: index_catalogo_prod_servs_on_clase_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_clase_prod_serv_id ON catalogo_prod_servs USING btree (clase_prod_serv_id);


--
-- Name: index_catalogo_prod_servs_on_clave_prod_serv; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_catalogo_prod_servs_on_clave_prod_serv ON catalogo_prod_servs USING btree (clave_prod_serv);


--
-- Name: index_catalogo_prod_servs_on_division_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_division_prod_serv_id ON catalogo_prod_servs USING btree (division_prod_serv_id);


--
-- Name: index_catalogo_prod_servs_on_fecha_fin_vigencia; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_fecha_fin_vigencia ON catalogo_prod_servs USING btree (fecha_fin_vigencia);


--
-- Name: index_catalogo_prod_servs_on_fecha_inicio_vigencia; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_fecha_inicio_vigencia ON catalogo_prod_servs USING btree (fecha_inicio_vigencia);


--
-- Name: index_catalogo_prod_servs_on_grupo_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_grupo_prod_serv_id ON catalogo_prod_servs USING btree (grupo_prod_serv_id);


--
-- Name: index_catalogo_prod_servs_on_tipo; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_catalogo_prod_servs_on_tipo ON catalogo_prod_servs USING btree (tipo);


--
-- Name: index_clase_prod_servs_on_clave_clase; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_clase_prod_servs_on_clave_clase ON clase_prod_servs USING btree (clave_clase);


--
-- Name: index_clase_prod_servs_on_division_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_clase_prod_servs_on_division_prod_serv_id ON clase_prod_servs USING btree (division_prod_serv_id);


--
-- Name: index_clase_prod_servs_on_grupo_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_clase_prod_servs_on_grupo_prod_serv_id ON clase_prod_servs USING btree (grupo_prod_serv_id);


--
-- Name: index_clase_prod_servs_on_tipo; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_clase_prod_servs_on_tipo ON clase_prod_servs USING btree (tipo);


--
-- Name: index_division_prod_servs_on_clave_division; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_division_prod_servs_on_clave_division ON division_prod_servs USING btree (clave_division);


--
-- Name: index_division_prod_servs_on_tipo; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_division_prod_servs_on_tipo ON division_prod_servs USING btree (tipo);


--
-- Name: index_grupo_prod_servs_on_clave_grupo; Type: INDEX; Schema: public; Owner: -
--

CREATE UNIQUE INDEX index_grupo_prod_servs_on_clave_grupo ON grupo_prod_servs USING btree (clave_grupo);


--
-- Name: index_grupo_prod_servs_on_division_prod_serv_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_grupo_prod_servs_on_division_prod_serv_id ON grupo_prod_servs USING btree (division_prod_serv_id);


--
-- Name: index_grupo_prod_servs_on_tipo; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_grupo_prod_servs_on_tipo ON grupo_prod_servs USING btree (tipo);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user", public;

INSERT INTO "schema_migrations" (version) VALUES
('20170905180729'),
('20170905193435'),
('20170905194026'),
('20170905194311'),
('20170907185042'),
('20170907185949'),
('20170907190901'),
('20170918172927'),
('20170918183549'),
('20170918184702'),
('20170918234504'),
('20170919181958'),
('20170920000030'),
('20170920045913'),
('20170920185710'),
('20170929184745'),
('20171025165801'),
('20171025212308');


