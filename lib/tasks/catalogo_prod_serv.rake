namespace :catalogo_prod_serv do
  desc 'Crea nuevos registros agregados al catalogo de productos y servicios el 18-Nov-2017'
  task nuevos_registros: :environment do
    process_excel('db/c_ClaveProdServ_18-Nov-2017.xlsx')
  end

  desc 'Actualiza la descripcion_clase de registros a los que su clase ya no tiene descripcion vacia segun catalogo 18-Nov-2017'
  task actualizar_descripcion_clase: :environment do
    claves_clase = %w[
      50211500
      78111800
      84131600
      84131800
      85101500
      85101600
      85101700
      85121500
    ]

    claves_clase.each do |item|
      clase = ClaseProdServ.find_by(clave_clase: item[0, 6])
      puts "Actualizando descripcion_clase de productos con clase id #{clase.id}"
      CatalogoProdServ.where(clase_prod_serv_id: clase.id).update_all(descripcion_clase: clase.descripcion)
    end
  end
end

def process_excel(file_path)
  require 'creek'
  creek = Creek::Book.new(file_path)
  sheet = creek.sheets[0]

  header = nil
  sheet.rows_with_meta_data.each do |row|
    header = row['cells'].values
    break
  end

  header.map!(&:parameterize)

  sheet.rows_with_meta_data.each do |row|
    next if row['r'].to_i <= 1
    r = Hash[header.zip(row['cells'].values)]
    c_clave_prodserv = r['c_claveprodserv'].to_s.gsub(/\.0/, '')

    producto = CatalogoProdServ.select(:id).where(clave_prod_serv: c_clave_prodserv)

    next unless c_clave_prodserv.present? && producto.blank?

    puts "'#{c_clave_prodserv}',"
    CatalogoProdServ.new.tap do |ps|
      ps.clave_prod_serv = c_clave_prodserv
      ps.descripcion = r['descripcion'].tr("\n", ' ')
      ps.fecha_inicio_vigencia = r['fechainiciovigencia']
      ps.fecha_fin_vigencia = r['fechafinvigencia']
      ps.incluir_iva_trasladado = r['incluir-iva-trasladado']
      ps.incluir_ieps_trasladado = r['incluir-ieps-trasladado']
      ps.complemento_debe_incluir = r['complemento-que-debe-incluir']

      # Obtener Division, Grupo y Clase, descripcionese id correspondientes
      division = DivisionProdServ.where(clave_division: c_clave_prodserv[0, 2]).first
      ps.division_prod_serv_id = division.id if division.present?
      ps.descripcion_division = division.present? ? division.descripcion : ''
      ps.tipo = division.tipo if division.present?

      grupo = GrupoProdServ.where(clave_grupo: c_clave_prodserv[0, 4]).first
      ps.grupo_prod_serv_id = grupo.id if grupo.present?
      ps.descripcion_grupo = grupo.present? ? grupo.descripcion : ''

      clase = ClaseProdServ.where(clave_clase: c_clave_prodserv[0, 6]).first
      ps.clase_prod_serv_id = clase.id if clase.present?
      ps.descripcion_clase = clase.present? ? clase.descripcion : ''
      ps.save!
    end
  end
end
