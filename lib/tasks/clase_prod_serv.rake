namespace :clase_prod_serv do
  desc 'Actualiza la descripcion de clases agregadas al catalogo de productos y servicios el 18-Nov-2017'
  task actualizar_registros: :environment do
    # Ojo, actualiza porque en realidad estas clases ya existian en ClaseProdServ pero tenian descripcion
    # vacias, porque existian en el buscador del SAT y no en el excel del catalogo.

    # 50211500 - Tabacos, cigarros y cigarrillos
    # 78111800 - Transporte de pasajeros por tierra
    # 84131600 - Servicios de aseguradoras
    # 84131800 - Servicios de administradoras de fondos para el retiro
    # 85101500 - Servicios de hospitales, clínicas y consultorios
    # 85101600 - Servicios para el cuidado de la salud
    # 85101700 - Servicios de salud pública
    # 85121500 - Honorarios médicos
    new_items = [
      { '50211500' => 'Tabacos, cigarros y cigarrillos' },
      { '78111800' => 'Transporte de pasajeros por tierra' },
      { '84131600' => 'Servicios de aseguradoras' },
      { '84131800' => 'Servicios de administradoras de fondos para el retiro' },
      { '85101500' => 'Servicios de hospitales, clínicas y consultorios' },
      { '85101600' => 'Servicios para el cuidado de la salud' },
      { '85101700' => 'Servicios de salud pública' },
      { '85121500' => 'Honorarios médicos' }
    ]
    new_items.each do |item|
      clave_clase = item.first[0]
      descripcion_clase = item.first[1]

      ClaseProdServ.find_or_initialize_by(clave_clase: clave_clase[0, 6]).tap do |clase|
        # Obtener Division, Grupo y Clase, descripcionese id correspondientes
        division = DivisionProdServ.where(clave_division: clave_clase[0, 2]).first
        grupo = GrupoProdServ.where(clave_grupo: clave_clase[0, 4]).first

        clase.division_prod_serv_id = division.id if division.present?
        clase.tipo = division.tipo if division.present?
        clase.grupo_prod_serv_id = grupo.id if grupo.present?
        clase.descripcion = descripcion_clase

        clase.save!
        if clase.new_record?
          puts "Se creo registro para clave_clase: #{clave_clase[0, 6]}"
        else
          puts "Se actualizo registro para clave_clase: #{clave_clase[0, 6]}"
        end
      end
    end
  end

  desc 'TODO'
  task actualizar_descripciones: :environment do
    puts 'ToDo'
  end
end
