RSpec.shared_examples_for 'Pagination' do
  let(:obj) { described_class.new }
  let(:page_size_default) { 20 }

  describe '#pagination_per_page' do
    context 'when page_size inside limits' do
      it 'returns same page_size' do
        expect(obj.pagination_per_page(5)).to eq(5)
      end
    end

    context 'when page_size is over pagiation limit' do
      it 'returns pagination default' do
        expect(obj.pagination_per_page(150)).to eq(page_size_default)
      end
    end

    context 'when page_size is 0 or negative' do
      it 'returns pagination default' do
        expect(obj.pagination_per_page(0)).to eq(page_size_default)
        expect(obj.pagination_per_page(-1)).to eq(page_size_default)
      end
    end

    context 'when page_size is not present' do
      it 'returns pagination default' do
        expect(obj.pagination_per_page(nil)).to eq(page_size_default)
        expect(obj.pagination_per_page([])).to eq(page_size_default)
        expect(obj.pagination_per_page('')).to eq(page_size_default)
      end
    end
  end
end
