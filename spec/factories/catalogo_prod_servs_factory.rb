FactoryBot.define do
  factory :catalogo_prod_serv do
    clase_prod_serv
    grupo_prod_serv { clase_prod_serv.grupo_prod_serv }
    division_prod_serv { clase_prod_serv.division_prod_serv }

    clave_prod_serv '12345678'
    descripcion 'desc'
    fecha_inicio_vigencia '2017-06-06'
  end
end
