FactoryBot.define do
  factory :clase_prod_serv do
    grupo_prod_serv
    division_prod_serv { grupo_prod_serv.division_prod_serv }

    clave_clase '88'
    descripcion 'desc'
  end
end
