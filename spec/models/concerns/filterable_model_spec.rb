require 'rails_helper'

RSpec.describe 'FilterableModel' do
  describe '.filter' do
    let(:a_filterable_model) do
      Class.new(ApplicationRecord) do
        include FilterableModel

        scope :descripcion, ->(_text) { where(nil) }
        scope :fecha_inicio_vigencia, ->(_text) { where(nil) }
        scope :fecha_fin_vigencia, ->(_text) { where(nil) }
      end
    end

    let(:white_list) do
      %w[
        descripcion
        fecha_inicio_vigencia
        fecha_fin_vigencia
      ]
    end

    context 'filtering_params contain a filter not in the white_list' do
      let(:white_list) { %w[descripcion fecha_fin_vigencia] }
      let(:filtering_params) do
        [
          '{"id":"descripcion","value":"%dude%"}',
          '{"id":"fecha_inicio_vigencia","value":{"inicial":"2018-04-02","final":""}}',
          '{"id":"fecha_fin_vigencia","value":{"inicial":"","final":"2018-06-09"}}'
        ]
      end

      it 'calls only the scopes allowed in the white_list' do
        expect(a_filterable_model).to receive(:descripcion).with('%dude%').and_call_original
        expect(a_filterable_model).to receive(:fecha_fin_vigencia).and_call_original
        expect(a_filterable_model).not_to receive(:fecha_inicio_vigencia).and_call_original

        a_filterable_model.filter(filtering_params, white_list)
      end
    end

    context 'filtering_params contain filters in the white_list but with invalid values' do
      let(:filtering_params) do
        [
          '{"id":"descripcion","value":"%%"}',
          '{"id":"fecha_inicio_vigencia","value":"%"}',
          '{"id":"fecha_fin_vigencia","value":""}'
        ]
      end

      it 'does not call any of those scopes' do
        expect(a_filterable_model).not_to receive(:descripcion).and_call_original
        expect(a_filterable_model).not_to receive(:fecha_inicio_vigencia).and_call_original
        expect(a_filterable_model).not_to receive(:fecha_fin_vigencia).and_call_original

        a_filterable_model.filter(filtering_params, white_list)
      end
    end
  end
end
