RSpec.shared_examples_for 'FilterableModel' do
  describe '.filter_date_range' do
    let(:subject) { described_class.filter_date_range(column_name, date_range) }
    let(:column_name) { 'fecha_inicio_vigencia' }

    context 'date_range with start and final dates defined' do
      let(:date_range) do
        { 'inicial' => '2018-06-09', 'final' => '2018-11-09' }
      end

      it 'returns expected scope' do
        expect(subject).to eq(
          described_class.where(column_name => '2018-06-09'..'2018-11-09')
        )
      end
    end

    context 'date_range with only start date defined' do
      let(:date_range) do
        { 'inicial' => '2018-06-09', 'final' => '' }
      end

      it 'returns expected scope' do
        expect(subject).to eq(
          described_class.where(described_class.arel_table[column_name].gteq('2018-06-09'))
        )
      end
    end

    context 'date_range with only final date defined' do
      let(:date_range) do
        { 'inicial' => '', 'final' => '2018-06-09' }
      end

      it 'returns expected scope' do
        expect(subject).to eq(
          described_class.where(described_class.arel_table[column_name].lteq('2018-06-09'))
        )
      end
    end
  end
end
