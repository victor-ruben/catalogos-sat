require 'rails_helper'

RSpec.describe 'SortableModel' do
  describe '.add_order' do
    let(:a_sortable_model) do
      Class.new(ApplicationRecord) do
        include SortableModel

        def order(_hash); end
      end
    end

    context 'param does not contain a valid sort specified' do
      let(:invalid_param) { ['{"invalidkey":"id","desc":false}'] }

      it 'calls order with a default sort hash' do
        expect(a_sortable_model).to receive(:order).with(
          id: :asc
        ).exactly(5).times

        a_sortable_model.add_order(nil)
        a_sortable_model.add_order([])
        a_sortable_model.add_order({})
        a_sortable_model.add_order('')
        a_sortable_model.add_order(invalid_param)
      end
    end

    context 'param contains a valid sort specified' do
      let(:param) { ['{"id":"descripcion","desc":true}'] }

      it 'calls order with the expected hash' do
        expect(a_sortable_model).to receive(:order).with(
          descripcion: :desc,
          id: :desc
        )

        a_sortable_model.add_order(param)
      end
    end

    context 'param contains more that one valid sort specified' do
      let(:param) do
        [
          '{"id":"dude","desc":false}',
          '{"id":"descripcion","desc":true}'
        ]
      end

      it 'calls order only with the first valid sort' do
        expect(a_sortable_model).to receive(:order).with(
          dude: :asc,
          id: :asc
        )

        a_sortable_model.add_order(param)
      end
    end

    # TODO: remove when/if tipo column is renamed to tipo_id in the catalogo_prod_servs table
    context 'param contain an id with tipo_id' do
      let(:param) { ['{"id":"tipo_id","desc":false}'] }

      it 'calls order with tipo instead of tipo_id' do
        expect(a_sortable_model).to receive(:order).with(
          tipo: :asc,
          id: :asc
        )

        a_sortable_model.add_order(param)
      end
    end
  end
end
