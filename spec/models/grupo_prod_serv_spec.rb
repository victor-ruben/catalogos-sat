require 'rails_helper'

RSpec.describe GrupoProdServ, type: :model do
  describe 'scope filter_by_descripcion' do
    let(:grupo) { FactoryBot.create(:grupo_prod_serv) }
    let(:results) do
      GrupoProdServ.filter_by_descripcion(grupo.descripcion[0..4]).first
    end

    it 'filters correctly' do
      expect(grupo.id).to eq(results.id)
      expect(grupo.descripcion).to eq(results.descripcion)
    end
  end

  describe '.list_items' do
    let(:where_values_hash) do
      GrupoProdServ.list_items(filters).where_values_hash
    end

    context 'when no filters passed' do
      let(:filters) { nil }

      it 'returns scope without filters' do
        expect(where_values_hash).to eq({})
      end
    end

    context 'when invalid filters passed' do
      let(:filters) { { 'dude' => 44 } }

      it 'returns scope without filters' do
        expect(where_values_hash).to eq({})
      end
    end

    context 'when valid and invalid filters passed' do
      let(:filters) do
        { 'division_id' => 887,
          'invalid' => 'jkjkjk',
          'tipo_id' => '1',
          'invalid2' => 0o03 }
      end

      it 'returns scope with the valid filters' do
        expect(where_values_hash.size).to eq(2)
        expect(where_values_hash).to eq(
          'division_prod_serv_id' => 887, 'tipo' => '1'
        )
      end
    end
  end
end
