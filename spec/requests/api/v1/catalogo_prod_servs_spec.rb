require 'rails_helper'

RSpec.describe 'v1/catalogo_prod_servs endpoint', type: :request do
  let(:json) { JSON.parse(response.body) }

  context 'no records in catalogo_prod_servs' do
    context 'request without params' do
      let(:get_catalogo_prod_servs) { get '/v1/catalogo_prod_servs' }

      it 'responds success with no data' do
        get_catalogo_prod_servs

        expect(response.status).to eq(200)
        expect(json['data'].length).to eq(0)
        expect(json.dig('meta', 'pagination', 'total_objects')).to eq(0)
      end
    end
  end

  context 'at least one record in catalogo_prod_servs' do
    shared_examples 'catalogo_prod_serv response' do
      it 'return expected json body' do
        expect(json['data'].length).to eq(1)
        expect(json['data'][0]['type']).to eq('catalogo_prod_serv')
      end
    end

    let(:catalogo_prod_serv) do
      FactoryBot.create(:catalogo_prod_serv, descripcion: record_desc)
    end
    let(:get_catalogo_prod_servs) { get "/v1/catalogo_prod_servs?#{search_params}" }

    let(:search_params) do
      URI.encode_www_form(
        'filtered[]' => ["{\"id\":\"descripcion\",\"value\":\"#{search_text}\"}"]
      )
    end
    let(:json_data_desc) { json.dig('data', 0, 'attributes', 'descripcion') }

    before do
      catalogo_prod_serv
      get_catalogo_prod_servs
    end

    describe 'upper / lowercase search' do
      context 'request contain params searching something all uppercase' do
        it_behaves_like 'catalogo_prod_serv response'

        let(:record_desc) { 'some text with no accent division' }
        let(:search_text) { '%DIVISION%' }

        it 'find existing record' do
          expect(json_data_desc).to eq(catalogo_prod_serv.descripcion)
        end
      end

      context 'request contain params searching something all lowercase' do
        it_behaves_like 'catalogo_prod_serv response'

        let(:record_desc) { 'some text with no accent DIVISION' }
        let(:search_text) { '%division%' }

        it 'find existing record' do
          expect(json_data_desc).to eq(catalogo_prod_serv.descripcion)
        end
      end
    end

    describe 'accent search' do
      context 'request contain params searching something with accent' do
        it_behaves_like 'catalogo_prod_serv response'

        let(:record_desc) { 'some text with no accent division' }
        let(:search_text) { '%división%' }

        it 'find existing record with no accent' do
          expect(json_data_desc).to eq(catalogo_prod_serv.descripcion)
        end
      end

      context 'request contain params searching something without accent' do
        it_behaves_like 'catalogo_prod_serv response'

        let(:record_desc) { 'some text with accent división' }
        let(:search_text) { '%division%' }

        it 'find existing record with accent' do
          expect(json_data_desc).to eq(catalogo_prod_serv.descripcion)
        end
      end
    end
  end
end
