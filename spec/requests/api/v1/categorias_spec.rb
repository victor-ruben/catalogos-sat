require 'rails_helper'

RSpec.describe 'v1/categoria/:categoria endpoint', type: :request do
  let(:json) { JSON.parse(response.body) }

  describe '#index' do
    context 'invalid categoria param is requested ' do
      let(:get_category) { get '/v1/categoria/invalid_djh' }

      it 'responds 404 with error message' do
        get_category
        expect(response.status).to eq(404)
        expect(json['error']). to eq('No encontrado')
      end
    end

    context 'division category requested' do
      let(:division_prod_serv) do
        FactoryBot.create(:division_prod_serv)
      end
      let(:get_category) { get '/v1/categoria/division' }

      it 'responds success' do
        get_category
        expect(response.status).to eq(200)
      end

      it 'responds with expected json body' do
        division_prod_serv
        get_category

        expect(json['data'].length).to eq(1)
        expect(json['data'][0]['type']).to eq('division_prod_serv')
        expect(json['data'][0]['attributes']['descripcion']).to eq(
          division_prod_serv.descripcion
        )
      end
    end

    context 'grupo category requested' do
      let(:get_category) { get '/v1/categoria/grupo' }

      it 'responds success' do
        get_category
        expect(response.status).to eq(200)
      end

      # TODO
      xit 'responds with expected json body' do
      end
    end

    context 'clase category requested' do
      let(:get_category) { get '/v1/categoria/clase' }

      it 'responds success' do
        get_category
        expect(response.status).to eq(200)
      end

      # TODO
      xit 'responds with expected json body' do
      end
    end
  end
end
